# **Explorando las nuevos features de GitHub**

![github](https://user-images.githubusercontent.com/23284899/147618103-4715b586-df1e-4737-b863-b4cfdba80d92.png)

*__GitHub__ es la red social del código más famosa por muchos desarrolladores y en estos últimas semanas ha implementado nuevas novedades que derrepente sean de tu interés 👀, si quieres participar solo únete al team nos vemos el 28 de diciembre.* 🤓

## __Speaker:__

|Speaker | Profesión | Afiliación
---|---|---
Antony Barja | Geógrafo freelance | <li>Miembro oficial de QGIS Perú</li><br/><li>Investigador en formación dentro de HealthInnovation Lab - UPCH</li>

## __Mi primer usuario en GitHub__

![profile](https://user-images.githubusercontent.com/23284899/147617086-d8f4864d-b089-47d3-b950-5a3fd1d93e1d.png)

## __Mi primer repositorio__
![crear_repositorio](https://user-images.githubusercontent.com/23284899/147617156-a63ffe9c-54ba-42fa-bdba-08a5c837254e.png)

## __Nuevos features:__

### 🔵 Apariencia:
 - Main readme:
    - Profile user 
    - Organization 
 - Color Palette
 - Colorbind Themes
 - Light High Contrast Theme
 - Nueva barra de características

### 🔵 Repositorio:
  - Discussions
  - starts:
     - Create list
  - Watch : 
     - Customize > check list    
  - Citation cff

### 🔵 Workspace 
  - CodeSpaces (vscodeweb - gitpod)   
  - Sponsors

## __Resultados__
![img01](https://gitlab.com/uploads/-/system/personal_snippet/2228250/bfce18a1d46da9e89553fffdc41abc1d/4Github_readme.jpg)

![img02](https://gitlab.com/uploads/-/system/personal_snippet/2228250/0e9a664768d6a970dc1168213cb011a0/3Github_citation.jpg)
